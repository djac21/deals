package com.djac21.deals.adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.djac21.deals.R
import com.djac21.deals.activities.MainActivity
import com.djac21.deals.databinding.ItemViewBinding
import com.djac21.deals.fragments.DetailsFragment
import com.djac21.deals.models.DataItem
import com.djac21.deals.utils.DEALS_MODEL
import com.djac21.deals.utils.Utils.urlFormat

class DealsAdapter(private var dealsList: List<DataItem>, private val context: Context) : RecyclerView.Adapter<DealsAdapter.ViewHolder>() {
    private var filteredDealsList: List<DataItem>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listItem = filteredDealsList[position]
        listItem.let { holder.bind(it) }

        Glide.with(context)
            .load(urlFormat(listItem.image))
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
            )
            .into(holder.itemViewDeals.image)
    }

    override fun getItemCount(): Int = filteredDealsList.size

    val filter: Filter
        get() = object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                filteredDealsList = if (charString.isEmpty()) {
                    dealsList
                } else {
                    val filteredList: MutableList<DataItem> = ArrayList()
                    for (item in dealsList) {
                        if (item.title.lowercase().contains(charString.lowercase()))
                            filteredList.add(item)
                    }

                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = filteredDealsList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                filteredDealsList = filterResults.values as ArrayList<DataItem>
                notifyDataSetChanged()
            }
        }

    inner class ViewHolder(var itemViewDeals: ItemViewBinding) : RecyclerView.ViewHolder(itemViewDeals.root), View.OnClickListener {
        fun bind(item: DataItem) {
            itemViewDeals.item = item
            itemViewDeals.executePendingBindings()
        }

        override fun onClick(view: View?) {
            val dealsList: DataItem = filteredDealsList[absoluteAdapterPosition]

            val detailsFragment = DetailsFragment()
            val bundle = Bundle()
            bundle.putSerializable(DEALS_MODEL, dealsList)
            detailsFragment.arguments = bundle

            (context as MainActivity).supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, detailsFragment)
                .addToBackStack(null)
                .commit()
        }

        init {
            itemView.setOnClickListener(this)
        }
    }

    init {
        filteredDealsList = dealsList
    }
}