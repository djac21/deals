package com.djac21.deals.api

import com.djac21.deals.models.DealsModel
import io.reactivex.Single
import retrofit2.http.GET

interface ApiInterface {
    @GET("deals")
    fun getDeals(): Single<DealsModel>?
}