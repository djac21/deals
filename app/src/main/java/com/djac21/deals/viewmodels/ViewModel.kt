package com.djac21.deals.viewmodels

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.djac21.deals.api.ApiClient
import com.djac21.deals.api.ApiInterface
import com.djac21.deals.models.DealsModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class ViewModel : ViewModel() {
    private var disposable: CompositeDisposable?
    private val liveData: MutableLiveData<DealsModel> = MutableLiveData<DealsModel>()
    private val dataError = MutableLiveData<Boolean>()
    private val loading = MutableLiveData<Boolean>()

    fun getLoading(): LiveData<Boolean> {
        return loading
    }

    fun getData(context: Context?): LiveData<DealsModel> {
        fetchRepos(context)
        return liveData
    }

    val error: LiveData<Boolean>
        get() = dataError

    private fun fetchRepos(context: Context?) {
        loading.value = true

        ApiClient.getRetrofit(context).create(ApiInterface::class.java).getDeals()
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSingleObserver<DealsModel?>() {
                override fun onSuccess(dealsModel: DealsModel) {
                    dataError.value = false
                    liveData.value = dealsModel
                    loading.value = false
                }

                override fun onError(e: Throwable) {
                    dataError.value = true
                    loading.value = false
                }
            })?.let {
                disposable?.add(it)
            }
    }

    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.clear()
            disposable = null
        }
    }

    init {
        disposable = CompositeDisposable()
    }
}