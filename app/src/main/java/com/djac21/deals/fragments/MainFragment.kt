package com.djac21.deals.fragments

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.djac21.deals.R
import com.djac21.deals.adapters.DealsAdapter
import com.djac21.deals.databinding.FragmentMainBinding
import com.djac21.deals.models.DealsModel
import com.djac21.deals.utils.DEALS_ITEMS
import com.djac21.deals.utils.Utils
import com.djac21.deals.viewmodels.ViewModel
import com.google.android.material.snackbar.Snackbar

class MainFragment : Fragment() {
    private lateinit var viewModel: ViewModel
    private lateinit var fragmentMainBinding: FragmentMainBinding
    private var dealsAdapter: DealsAdapter? = null
    private var dealsModel: DealsModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        fragmentMainBinding = FragmentMainBinding.inflate(inflater)
        return fragmentMainBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)

        if (savedInstanceState?.get(DEALS_ITEMS) == null)
            getDeals(false)

        fragmentMainBinding.swipeRefreshLayout.setOnRefreshListener {
            getDeals(true)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (dealsModel != null)
            outState.putSerializable(DEALS_ITEMS, dealsModel)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        dealsModel = savedInstanceState?.getSerializable(DEALS_ITEMS) as DealsModel?
        setUI()
    }

    private fun getDeals(swipeToRefresh: Boolean) {
        if (!Utils.isNetworkAvailable(context))
            Snackbar.make(fragmentMainBinding.swipeRefreshLayout, "Offline mode - No internet available", Snackbar.LENGTH_SHORT).show()

        viewModel.getLoading().observe(viewLifecycleOwner) { loading: Boolean? ->
            if (loading != null) {
                if (loading) {
                    if (!swipeToRefresh)
                        fragmentMainBinding.progressBar.visibility = View.VISIBLE

                    fragmentMainBinding.errorMessage.visibility = View.GONE
                    fragmentMainBinding.recyclerView.visibility = View.GONE
                } else {
                    fragmentMainBinding.progressBar.visibility = View.GONE
                }
            }
        }

        viewModel.getData(context).observe(viewLifecycleOwner) { dealsModelResponse: DealsModel? ->
            if (dealsModelResponse != null) {
                dealsModel = dealsModelResponse
                setUI()
            }
        }

        viewModel.error.observe(viewLifecycleOwner) { error: Boolean? ->
            if (error != null)
                if (error) {
                    fragmentMainBinding.errorMessage.visibility = View.VISIBLE
                    fragmentMainBinding.recyclerView.visibility = View.GONE
                } else {
                    fragmentMainBinding.errorMessage.visibility = View.GONE
                    fragmentMainBinding.recyclerView.visibility = View.VISIBLE
                }
            fragmentMainBinding.swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun setUI() {
        fragmentMainBinding.swipeRefreshLayout.isRefreshing = false
        fragmentMainBinding.progressBar.visibility = View.GONE

        dealsAdapter = dealsModel?.data?.let { context?.let { it1 -> DealsAdapter(it, it1) } }
        fragmentMainBinding.recyclerView.visibility = View.VISIBLE
        fragmentMainBinding.recyclerView.adapter = dealsAdapter
        fragmentMainBinding.recyclerView.layoutManager = LinearLayoutManager(context)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val searchMenuItem = menu.findItem(R.id.search_view)
        val searchView = searchMenuItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                dealsAdapter?.filter?.filter(query)
                return false
            }
        })
        super.onCreateOptionsMenu(menu, inflater)
    }

    companion object {
        fun newInstance() = MainFragment()
    }
}