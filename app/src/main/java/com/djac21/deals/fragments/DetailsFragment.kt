package com.djac21.deals.fragments

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.djac21.deals.R
import com.djac21.deals.databinding.FragmentDetailsBinding
import com.djac21.deals.models.DataItem
import com.djac21.deals.utils.DEALS_MODEL
import com.djac21.deals.utils.Utils

class DetailsFragment : Fragment() {
    private lateinit var fragmentDetailsBinding: FragmentDetailsBinding
    private var dataItem: DataItem? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentDetailsBinding = FragmentDetailsBinding.inflate(inflater)
        return fragmentDetailsBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataItem = requireArguments().getSerializable(DEALS_MODEL) as DataItem?

        fragmentDetailsBinding.items = dataItem

        if (dataItem?.salePrice == null)
            fragmentDetailsBinding.salesPrice.visibility = View.GONE
        else
            fragmentDetailsBinding.regularPrice.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

        Glide.with(this)
            .load(dataItem?.image?.let { Utils.urlFormat(it) })
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_no_image)
                    .error(R.drawable.ic_no_image)
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
            )
            .into(fragmentDetailsBinding.image)
    }
}