package com.djac21.deals.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DealsModel(
    @SerializedName("data")
    val data: List<DataItem>?,
    @SerializedName("_id")
    val id: String = "",
    @SerializedName("type")
    val type: String = ""
) : Serializable