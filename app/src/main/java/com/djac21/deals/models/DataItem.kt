package com.djac21.deals.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataItem(
    @SerializedName("image")
    val image: String = "",
    @SerializedName("salePrice")
    val salePrice: String = "",
    @SerializedName("price")
    val price: String = "",
    @SerializedName("description")
    val description: String = "",
    @SerializedName("guid")
    val guid: String = "",
    @SerializedName("index")
    val index: Int = 0,
    @SerializedName("_id")
    val id: String = "",
    @SerializedName("aisle")
    val aisle: String = "",
    @SerializedName("title")
    val title: String = ""
) : Serializable