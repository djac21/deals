package com.djac21.deals.utils

const val BASE_URL = "https://target-deals.herokuapp.com/api/"
const val DEALS_MODEL = "deals_model"
const val DEALS_ITEMS = "deals_items"